package es.unileon.procesadores.symbols;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Stack;

public class SymbolsTable {

	// Apuntar� a la tabla de s�mbolos local
	private HashMap t;
	// Cada vez que encontremos un nuevo bloque que anidar, lo tendremos aqu� con su tabla de s�mbolos propia
	private Stack bloques;
	
	// Un nuevo bloque que aparezca ser� situado en el top actual de la pila y tendr� asociada su tabla de s�mbolos propia
	
	
	public SymbolsTable() {
		HashMap tablaprincipal = new HashMap();
		bloques = new Stack();
		bloques.push(tablaprincipal);
		t = (HashMap)bloques.peek();
	 }
	 
	 public MySymbol insertar(MySymbol s) {
		 t.put(s.nombre, s);
		 return s;
	 }
	 
	 public MySymbol buscar(String nombre) {
		 return (MySymbol)(t.get(nombre));
	 }
	 
	 public MySymbol buscarGlobal(String nombre) {
		 HashMap tabla;
		 MySymbol s;
		 for (int i=(bloques.size() - 1); i >= 0; i--) {
			 tabla = (HashMap) bloques.elementAt(i);
			 //System.out.println("Buscando "+nombre+" en tabla "+(i+1));
			 s = (MySymbol) tabla.get(nombre);
			 if (s != null) {
				 //System.out.println("Encontrado "+nombre+" en tabla "+(i+1));
				 return s;
			 }
		 }
		 return null;
	 }
	 
	 public void set() {
		 HashMap nuevaTabla = new HashMap();
		 bloques.push(nuevaTabla);
		 t = (HashMap) bloques.peek();
	 }
	 
	 public void reset() {
		 bloques.pop();
		 t = (HashMap)bloques.peek();
	 }
	 
	 public void imprimir() {
		 for (int i = (bloques.size() - 1); i >= 0; i--) {
			 System.out.println("Bloque " + (i + 1) + ":");
			 HashMap tabla = (HashMap) bloques.elementAt(i);
			 Iterator it = tabla.values().iterator();
			 while(it.hasNext()) {
				 MySymbol s = (MySymbol) it.next();
				 System.out.println(s.nombre + "--> "+ s.valor);
			 }
		 }
	 }
	 
}
