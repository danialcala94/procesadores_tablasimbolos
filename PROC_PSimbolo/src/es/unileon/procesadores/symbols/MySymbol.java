package es.unileon.procesadores.symbols;

public class MySymbol {
	
	public String nombre;
	public Integer valor;
	
	public MySymbol(String nombre, Integer valor) {
		this.nombre = nombre;
		this.valor = valor;
	}
	
}
