package es.unileon.procesadores.makers;

/*
 * Autores: ALCAL� VALERA, DANIEL; CRESPO TORBADO, BEATRIZ; HERRERAS G�MEZ, FRANCISCO
 * Pr�ctica: GESTI�N DE TIPOS
 */

public class JFlexMaker {
	public static void generateParser(String path, String outputDir) {
		String args[] = {path, "-d", outputDir};
		jflex.Main.main(args);
	}
}
