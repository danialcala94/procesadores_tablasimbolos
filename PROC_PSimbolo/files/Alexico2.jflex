// Author: ALCAL� VALERA, DANIEL

package es.unileon.procesadores.parsers;

import java_cup.runtime.*;
import java.io.*;
import es.unileon.procesadores.symbols.*;
%%
%{
 private SymbolsTable tabla;
 public Yylex(Reader in, SymbolsTable t){
 this(in);
 this.tabla = t;
 }
 public SymbolsTable getTabla(){
 	return tabla;
 }
 public int linea(){return yyline+1;}
 public int columna(){ return yycolumn+1;}
 
%}
%unicode
%cup
%line
%column
%%
"+" { return new Symbol(sym.MAS); }
"-" { return new Symbol(sym.MENOS); }
"*" { return new Symbol(sym.POR); }
"/" { return new Symbol(sym.ENTRE); }
"%" { return new Symbol(sym.MODULO); }
";" { return new Symbol(sym.PTOYCOMA); }
"(" { return new Symbol(sym.ABREPAR); }
")" { return new Symbol(sym.CIERRAPAR); }
":=" { return new Symbol(sym.ASIGNA); }
"{" { 	return new Symbol(sym.INICIOBLOQUE); }
"}" { 	return new Symbol(sym.FINBLOQUE); }
[:jletter:][:jletterdigit:]* {
	MySymbol s;
	if ((s=tabla.buscar(yytext()))==null)
		// s = tabla.insertar(yytext()); Esto hab�a antes, pero petaba
		s = tabla.insertar(new MySymbol(yytext(), 0));
		return new Symbol(sym.ID,s);
	}
[:digit:]+ { return new Symbol(sym.NUM, new Integer(yytext())); }
[ \t\r\n]+ {;}
. { System.out.println("Error en l�xico."+yytext()+"-"); }
